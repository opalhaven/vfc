<?php
	class Request {
		public $url_elements;
		public $verb;
		public $parameters;
	 
		public function __construct() {
			$this->verb = $_SERVER['REQUEST_METHOD'];
			$this->url_elements =  explode('/',$_SERVER['ORIG_PATH_INFO']);
			$this->format = 'json';
 			$parameters = array();
	 
			if (isset($_SERVER['QUERY_STRING'])) {
				parse_str($_SERVER['QUERY_STRING'], $parameters);
			}
			$this->parameters = $parameters;
		}
	}
 	$request = new Request();
	/*
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');## required for AJAX
	header('Access-Control-Max-Age: 1000');## required for AJAX
	header('Access-Control-Allow-Credentials: false');## required for AJAX
	$http_origin            = $_SERVER['HTTP_ORIGIN']; ## required for AJAX 
	@header("Access-Control-Allow-Origin: " . $http_origin);
	*/

	echo json_encode($request);
?>
