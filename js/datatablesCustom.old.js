$(document).ready(function() {
  	//$(window).bind("resize", ReinitializeDatatable("", "", "", ""));

  	$( window ).resize(function() {
  		ReinitializeDatatable("", "", "", "")
  	});
});


function wraptable(original)
{
    original.wrap("<div class='table-wrapper' style='margin-bottom:0px' />");
    original.wrap("<div class='scrollable' />");
    original.find("td:nth-child(3), th:nth-child(3), td:nth-child(4), th:nth-child(4), td:nth-child(5), th:nth-child(5)").css("display", "none");
}

function ReinitializeDatatable(searchtxt, speakerid, placesid, year)
{
	

	/*If datatables is already initialized destroy it and request new data with new params*/

	var table = $.fn.dataTable.fnTables(true);
	    if ( table.length > 0 ) {
	      $(table).dataTable().fnDestroy();
	    }


	var srchtxt = null;
	var spid = null;
	var plcid = null;
	var yrid = null;

	var sParams = new Object();

	if(searchtxt){srchtxt = encodeURIComponent(searchtxt);}else{srchtxt=""}
	if(speakerid) {spid = speakerid;}else{spid = "";}
	if(placesid) {plcid = placesid;}else{plcid = "";}
	if(year) {yrid = year;}else{yrid =  "";}


	if (($(window).width() < 485))
    {
    	InitializeDTForMobile(srchtxt, spid, plcid, yrid);
    }
    else
    {
    	InitializeDT(srchtxt, spid, plcid, yrid);
    }

}


function InitializeDTForMobile(searchtxt, speakerid, placesid, year)
{
	$('#idspeaker').css("display", "none");
    $('#iddate').css("display", "none");
    $('#idplace').css("display", "none");

    var oTable = $(".dTable").dataTable(
    {
        bJQueryUI:!1,
        bAutoWidth:!1,
        /*bPaginate:false,*/
        bServerSide:true, 
        sScrollY:"600px",
        bDeferRender: true,
        oLanguage: {"sSearch": "Filter:"},
        oSearch: {"sSearch": searchtxt},
        sAjaxSource:"http://perry-vfc-api.azurewebsites.net/API/AudioMessage",
        fnServerParams: function ( aoData ) {aoData.push({"name":"SID","value":speakerid},{"name":"PID","value":placesid},{"name":"YEAR","value":year});},
        fnServerData: function ( sSource, aoData, fnCallback ) {
                    $.getJSON( sSource, aoData, function (json) {
                        /* Do whatever additional processing you want on the callback, then tell DataTables */
                        var transform = {"tag":"li","class":"active","style":"line-height:25px;","children":[
                                            {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","dataid":"${speaker_id}","children":[
                                                {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${SpeakerLastName}, ${SpeakerFirstName}   (${SPEAKERCOUNT})"}
                                              ]}
                                          ]}
                        var speakerData = json.fsData;
                        var top5data= [];
                        var i = 0;

                        $.each(json.fsData, function( key, value ) {
                                top5data[key] = value;
                                i++;
                                    if(i>6){
                                        return false;
                                    }
                                });

                        if(speakerData){
                            $('#filteredspeakers').empty();
                            $('#filteredspeakers').json2html(top5data,transform); 
                        }

                        var transformY = {"tag":"li","class":"active","style":"line-height:25px;","children":[
                                            {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","dataid":"${EventDate}","children":[
                                                {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${EventDate}   (${YEARCOUNT})"}
                                              ]}
                                          ]}
                        var yearData = json.fdData;

                        var top5yeardata= [];
                        i = 0;

                        $.each(json.fdData, function( key, value ) {
                                top5yeardata[key] = value;
                                i++;
                                    if(i>3){
                                        return false;
                                    }
                                });

                        if(yearData){
                            $('#filteredyears').json2html(top5yeardata,transformY);
                        }

                        var transformP = {"tag":"li","class":"active","style":"line-height:25px;","children":[
	                                        {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","dataid":"${place_id}","children":[
	                                            {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${EventPlace}   (${PLACECOUNT})"}
	                                          ]}
	                                      ]}
                        var placeData = json.fpData;

                        var top5placedata= [];
                        i = 0;

                        $.each(json.fpData, function( key, value ) {
                                top5placedata[key] = value;
                                i++;
                                    if(i>3){
                                        return false;
                                    }
                                });

                        if(placeData){
                            $('#filteredplaces').json2html(top5placedata,transformP); 
                        }

                        fnCallback(json)
                    })
                },  
        sPaginationType:"full_numbers",
        sDom:'t<ip>S',
        aoColumnDefs: [{ "bSortable": false, "aTargets": [ 0 ] }, { "sWidth": "45px", "aTargets": [ 0 ] }],
        aoColumns: [
                        {
                        "mData" : "ID",
                        "fnRender": function(obj) {
                            var sReturn = obj.aData[ "ID" ];

                            return "<ul class=\"the-icons\" style=\"margin:0px\"><li><a style=\"font-weight:bold\" href=\"\"><i class=\"icon-play\" style=\"font-size:12px\"></i>&nbsp;</a></li>" + "<li><a style=\"font-weight:bold\" href=\"\"><i class=\"icon-list-alt\" style=\"font-size:12px\"></i>&nbsp;+</a></li><li><a style=\"font-weight:bold\" href=\"http://voicesforchrist.org/audio_messages/"+sReturn+"?dl=true\"><i class=\"icon-cloud-download\" style=\"font-size:12px\"></i></li></ul>";


                            //return "<a href=\"http://voicesforchrist.org/audio_messages/"+sReturn+"?dl=true\" class=\"tag\">"+sReturn+"</a>";
                            }
                        },
                        {
                        "mData" : "SpeakerLastName",
                        "bVisible": false
                        },
                        {
                        "mData" : "Title",
                        "fnRender": function(obj) {
                            var Title = obj.aData[ "Title" ];
                            var Tags = obj.aData[ "Tags" ];
                                for (var i in Tags) {
                                Title += " <a href=\"http://www.google.com\">" + Tags[i] + "</a>"
                                }


                            var resp = "<div class=\"black\" style=\"padding-top:4px;\"><b><a href=\"\" class=\"blackNoLine\" target=\"_blank\">"+ Title + "</a></b>&nbsp;</div><div class=\"blackNoLine\" style=\"padding-top:4px;\">Speaker(s): <a href=\"\" class=\"blackNoLine\" target=\"_blank\">"+ obj.aData[ "SpeakerLastName" ] + "," + obj.aData[ "SpeakerFirstName" ] + "</a>&nbsp;</div>";

                            return resp;
                            }
                        },
                        {
                        "mData" : "EventDate",
                        "fnRender": function(obj) {
                                var EventDate = obj.aData[ "EventDate" ];
                                if ( EventDate) 
                                {
                                    return "<b>" + EventDate + "</b>";
                                }
                                return "<i>" + "unavailable" + "</i>";
                            },
                        "bVisible": false
                        },
                        { "mData" : "EventPlace",
                        "bVisible": false }
                    ] 
    }
    )

    $("table.responsive").each(function(i, element) {
        wraptable($(element));
      });

    oTable.bind('init', function () {

	      	$('#filteredspeakers > li > a').click(function() {
		        ReinitializeDatatable("", $(this).attr("dataid"), "" , "" );
		      });

		    $('#filteredplaces > li > a').click(function() {
		        ReinitializeDatatable("", "", $(this).attr("dataid") , "" );
		      });

		    $('#filteredyears > li > a').click(function() {
		        ReinitializeDatatable("", "" , "", $(this).attr("dataid") );
		      });

	      });

    //oTable.fnDraw();
	

}

function InitializeDT(searchtxt, speakerid, placesid, year)
{
	var oTable = $(".dTable").dataTable(
        {
            bJQueryUI:!1,
            bAutoWidth:!1,
            bServerSide:true, 
            sScrollY:"400px",
            bDeferRender: true,
            oLanguage: {"sSearch": "Filter:"},
            oSearch: {"sSearch": searchtxt},
            sAjaxSource:"http://perry-vfc-api.azurewebsites.net/API/AudioMessage",
            fnServerParams: function ( aoData ) {aoData.push( {"name":"SID","value":speakerid},{"name":"PID","value":placesid},{"name":"YEAR","value":year} );},
            fnServerData: function ( sSource, aoData, fnCallback ) {
                        $.getJSON( sSource, aoData, function (json) {
                            /* Do whatever additional processing you want on the callback, then tell DataTables */
                            var transform = {"tag":"li","class":"active","style":"line-height:25px;","children":[
                                            {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","dataid":"${speaker_id}","children":[
                                                {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${SpeakerLastName}, ${SpeakerFirstName}   (${SPEAKERCOUNT})"}
                                              ]}
                                          ]}
                            var speakerData = json.fsData;
                            var top5data= [];
                            var i = 0;

                            $.each(json.fsData, function( key, value ) {
                                    top5data[key] = value;
                                    i++;
                                        if(i>6){
                                            return false;
                                        }
                                    });

                            if(speakerData){
                                $('#filteredspeakers').empty();
                                $('#filteredspeakers').json2html(top5data,transform); 
                            }

                            var transformY = {"tag":"li","class":"active","style":"line-height:25px;","children":[
                                            {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","dataid":"${EventDate}","children":[
                                                {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${EventDate}   (${YEARCOUNT})"}
                                              ]}
                                          ]}
                            var yearData = json.fdData;

                            var top5yeardata= [];
                            i = 0;

                            $.each(json.fdData, function( key, value ) {
                                    top5yeardata[key] = value;
                                    i++;
                                        if(i>3){
                                            return false;
                                        }
                                    });

                            if(yearData){
                                $('#filteredyears').empty();
                                $('#filteredyears').json2html(top5yeardata,transformY);
                            }

                            var transformP = {"tag":"li","class":"active","style":"line-height:25px;","children":[
                                                {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","dataid":"${place_id}","children":[
                                                    {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${EventPlace}   (${PLACECOUNT})"}
                                                  ]}
                                              ]}
                            var placeData = json.fpData;

                            var top5placedata= [];
                            i = 0;

                            $.each(json.fpData, function( key, value ) {
                                    top5placedata[key] = value;
                                    i++;
                                        if(i>3){
                                            return false;
                                        }
                                    });

                            if(placeData){
                                $('#filteredplaces').empty();
                                $('#filteredplaces').json2html(top5placedata,transformP); 
                            }

                            fnCallback(json)
                        })
                    },  
            sPaginationType:"full_numbers",
            sDom:'t<"table-footer"ip>S',
            aoColumnDefs: [{ "bSortable": false, "aTargets": [ 0 ] }, { "sWidth": "102px", "aTargets": [ 0 ] }],
            aoColumns: [
                            {
                            "mData" : "ID",
                            "fnRender": function(obj) {
                                var sReturn = obj.aData[ "ID" ];

                                return "<a style=\"font-weight:bold\" href=\"\"><i class=\"icon-play\"></i>&nbsp;</a>" + "&nbsp;&nbsp;<a style=\"font-weight:bold\" href=\"\"><i class=\"icon-list-alt\"></i>&nbsp;+</a> &nbsp;&nbsp;<a style=\"font-weight:bold\" href=\"http://voicesforchrist.org/audio_messages/"+sReturn+"?dl=true\"><i class=\"icon-cloud-download\"></i>&nbsp;";

                                /*return "<a href=\"http://voicesforchrist.org/audio_messages/"+sReturn+"?dl=true\" class=\"tag\">"+sReturn+"</a>";*/
                                }
                            },
                            {
                            "mData" : "SpeakerLastName",
                            "fnRender": function(obj) {
                                var lastName = obj.aData[ "SpeakerLastName" ];
                                var firstName = obj.aData[ "SpeakerFirstName" ];
                                return lastName + "," + firstName;
                                }
                            },
                            {
                            "mData" : "Title",
                            "fnRender": function(obj) {
                                var Title = obj.aData[ "Title" ];
                                var Tags = obj.aData[ "Tags" ];
                                    for (var i in Tags) {
                                    Title += " <a href=\"http://www.google.com\">" + Tags[i] + "</a>"
                                    }
                                return Title;
                                }
                            },
                            {
                            "mData" : "EventDate",
                            "fnRender": function(obj) {
                                    var EventDate = obj.aData[ "EventDate" ];
                                    if ( EventDate) 
                                    {
                                        return "<b>" + EventDate + "</b>";
                                    }
                                    return "<i>" + "unavailable" + "</i>";
                                }
                            },
                            { "mData" : "EventPlace" }
                        ] 
        }
        )

	oTable.bind('init', function () {

	      	$('#filteredspeakers > li > a').click(function() {
		        ReinitializeDatatable("", $(this).attr("dataid"), "" , "" );
		      });

		    $('#filteredplaces > li > a').click(function() {
		        ReinitializeDatatable("", "", $(this).attr("dataid") , "" );
		      });

		    $('#filteredyears > li > a').click(function() {
		        ReinitializeDatatable("", "" , "", $(this).attr("dataid") );
		      });

	      });

            //oTable.fnDraw();

}