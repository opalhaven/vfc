$(function(){
	var e;
	$.uniform.defaults.fileButtonHtml="+",
	$.extend(
		$.fn.dataTableExt.oStdClasses,
		{
			sWrapper:"dataTables_wrapper form-inline"
		}
		),
	$(".dTable").dataTable(
		{
			bJQueryUI:!1,
			bAutoWidth:!1,
			bServerSide:true, 
			sAjaxSource:"http://localhost/SampleVCJson.txt",
			fnServerData: function ( sSource, aoData, fnCallback ) {
			            $.getJSON( sSource, aoData, function (json) {
			                /* Do whatever additional processing you want on the callback, then tell DataTables */
			                var transform = {"tag":"li","class":"active","style":"line-height:25px;","children":[
											    {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","children":[
											        {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${SpeakerLastName}, ${SpeakerFirstName}   (${Count})"}
											      ]}
											  ]}
			                var speakerData = json.fsData;
			                if(speakerData){
			                	$('#filteredspeakers').json2html(speakerData,transform); 
			            	}

			                var transformY = {"tag":"li","class":"active","style":"line-height:25px;","children":[
											    {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","children":[
											        {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${Year}   (${Count})"}
											      ]}
											  ]}
			                var yearData = json.fyData;
			                if(yearData){
			                	$('#filteredyears').json2html(yearData,transformY);
			                }

			                var transformP = {"tag":"li","class":"active","style":"line-height:25px;","children":[
											    {"tag":"a","href":"#","style":"margin-bottom: 0px; padding: 5px 15px; margin-left: -7px; margin-right: 0px;","children":[
											        {"tag":"span","style":"font-size: 12px; margin-left: 0px;","html":"${EventPlace}   (${Count})"}
											      ]}
											  ]}
			                var placeData = json.fpData;
			                if(placeData){
			                	$('#filteredplaces').json2html(placeData,transformP); 
			                }

			                fnCallback(json)
			            })
			        },  
			sPaginationType:"full_numbers",
			sDom:'<"table-header"fl>t<"table-footer"ip>',
			aoColumns: [
							{
							"mData" : "ID",
							"fnRender": function(obj) {
								var sReturn = obj.aData[ "ID" ];
								return "<a href=\"http://voicesforchrist.org/audio_messages/"+sReturn+"?dl=true\" class=\"tag\">"+sReturn+"</a>";
								}
							},
							{
							"mData" : "SpeakerLastName",
							"fnRender": function(obj) {
								var lastName = obj.aData[ "SpeakerLastName" ];
								var firstName = obj.aData[ "SpeakerFirstName" ];
								return lastName + "," + firstName;
								}
							},
							{
							"mData" : "Title",
							"fnRender": function(obj) {
								var Title = obj.aData[ "Title" ];
								var Tags = obj.aData[ "Tags" ];
									for (var i in Tags) {
									Title += " <a href=\"http://www.google.com\">" + Tags[i] + "</a>"
									}
								return Title;
								}
							},
							{
							"mData" : "EventDate",
							"fnRender": function(obj) {
									var EventDate = obj.aData[ "EventDate" ];
									if ( EventDate) 
									{
										return "<b>" + EventDate + "</b>";
									}
									return "<i>" + "unavailable" + "</i>";
								}
							},
							{ "mData" : "EventPlace" }
						] 
		}
		),
	$(".dTable-small").dataTable(
		{
			iDisplayLength:5,
			bJQueryUI:!1,
			bAutoWidth:!1,
			sPaginationType:"full_numbers",
			sDom:'<"table-header"fl>t<"table-footer"ip>',
			aoColumnDefs: [{ "bSortable": false, "aTargets": [ 0 ] }, { "sWidth": "58px", "aTargets": [ 0 ] }]
		}
		),
	$("select.uniform, input:file, .dataTables_length select").uniform()
})